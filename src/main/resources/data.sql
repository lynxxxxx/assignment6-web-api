INSERT INTO characterr (character_name, character_alias,character_gender,character_picture)
VALUES ('Peter Parker','Spiderman','Male', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOQAtytLaCFtg1cCHY_E5w5oeGm7TZxbqrUA&usqp=CAU');

INSERT INTO characterr (character_name, character_alias,character_gender,character_picture)
VALUES ('Bruce Wayne','Batman','Male', 'https://static.partyking.org/fit-in/1300x0/products/original/batman-supreme-maskeraddrakt-2.jpg');

INSERT INTO characterr (character_name, character_alias,character_gender,character_picture)
VALUES ('Clark Joseph Kent','Superman','Male', 'https://media.lekia.se/lekia/images/g-maki-6056778-2021-05-06-095255673/555/555/0/superman-actionfigur-30-cm.jpg');

INSERT INTO movie (movie_director, genre, movie_title, movie_picture, movie_trailer, release_year, franchise_id)
VALUES ('Zack Snyder','Action/Adventure','Batman v Superman: Dawn of Justice',
        'https://d2iltjk184xms5.cloudfront.net/uploads/photo/file/394027/70899aaeab56bb442ca79234bdf2546d-bvs.jpg', 'https://www.youtube.com/watch?v=0WWzgGyAH6Y','23.03.2016', null);

INSERT INTO movie (movie_director, genre, movie_title, movie_picture, movie_trailer, release_year, franchise_id)
VALUES ('Zack Snyder','Action/Adventure','Man of Steel', 'https://images-na.ssl-images-amazon.com/images/S/pv-target-images/badae1ac10aefce305002b37b6201c458df471825970b8083702bef47dc85914._RI_V_TTW_.jpg',
'https://www.youtube.com/watch?v=T6DJcgm3wNY','10.06.2013', null);

INSERT INTO movie (movie_director, genre, movie_title, movie_picture, movie_trailer, release_year, franchise_id)
VALUES ('Sam Raimi','Action/Fantasyfilm','Spider-man','https://c.flikshost.com/60004481/backdrops/large/d4bebb9b21572c494b95a45094efe30abbd4476b.jpg',
        'https://www.youtube.com/watch?v=t06RUxPbp_c','23.06.2002', null);


INSERT INTO franchise (franchise_description, franchise_name)
VALUES ('DC Comics used to be one of the two largest American comics publishers, today DC Comics is part of the Time Warner group', 'DC');

INSERT INTO franchise (franchise_description, franchise_name)
VALUES ('Marvel Comics is an American media and entertainment company regarded as one of the “big two” publishers in the comics industry', 'Marvel');

INSERT INTO franchise (franchise_description, franchise_name)
VALUES ('one of the "Big Five" major American film studios, as well as a member of the Motion Picture Association', 'WB');

INSERT INTO character_movie (character_id, movie_id) VALUES (1,3);
INSERT INTO character_movie (character_id, movie_id) VALUES (2,2);
INSERT INTO character_movie (character_id, movie_id) VALUES (3,2);


UPDATE movie SET franchise_id = 1 WHERE movie_id = 1;
UPDATE movie SET franchise_id = 2 WHERE movie_id = 3;
UPDATE movie SET franchise_id = 1 WHERE movie_id = 2;



