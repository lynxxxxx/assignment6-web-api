package com.example.assignment6webapi.dto;

import lombok.Value;

import java.util.List;

@Value
public class MovieDTO {


    Integer id;
    String movieTitle;
    String genre;
    String release_year;
    String director;
    String movie_picture;
    String movie_trailer;
   // FranchiseId franchiseIdCharacterId;
   // FranchiseDTO franchiseDTO;
    Integer franchise;
    List<Integer> characterIds;

}
