package com.example.assignment6webapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Value;

import java.util.List;

@Value
public class
CharacterDTO {
    Integer id;
    String fullName;
    String alias;
    String gender;
    String picture;
    List<Integer> movieIds;


}
