package com.example.assignment6webapi.dto;

import lombok.Value;

import java.util.List;


@Value
public class FranchiseDTO {
    Integer id;
    String franchiseName;
    String description;
    List<Integer> movieIds;
}
