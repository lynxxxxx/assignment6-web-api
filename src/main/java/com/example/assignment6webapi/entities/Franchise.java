package com.example.assignment6webapi.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Franchise {

    /**
     * A column for franchise table
     */
    @Id
    @Column(name = "franchise_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Integer id;

    @Column(name = "franchise_name", length = 50, nullable = false)
    private String franchiseName;

    @Column(name = "franchise_description", length = 200, nullable = false)
    private String description;

    /**
     * Basically a franchise can contain many movies
     */
    @OneToMany(mappedBy = "franchise",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Movie> movies;

    public void updateMovieToFranchise(Movie movie){
        movies.add(movie);
    }
}
