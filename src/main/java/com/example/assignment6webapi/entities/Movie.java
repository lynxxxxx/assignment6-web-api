package com.example.assignment6webapi.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Movie {
    @Id
    @Column(name = "movie_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Integer id;
    /**
     * Columns for movie table
     */

    @Column(name = "movie_title", length = 50, nullable = false)
    private String movieTitle;

    @Column(name = "genre", length = 50, nullable = false)
    private String genre;

    @Column(name = "release_year", length = 30, nullable = false)
    private String release_year;

    @Column(name = "movie_director", length = 50, nullable = false)
    private String director;

    @Column(name = "movie_picture", length = 150, nullable = false)
    private String movie_picture;

    @Column(name = "movie_trailer", length = 150, nullable = false)
    private String movie_trailer;

    /**
     * Movie belongs to one franchise
     */

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name= "franchise_id")
    @JsonIgnore
    private Franchise franchise;

    /**
     * Basically one movie contains many characters
     */
   @ManyToMany
   @JoinTable(name = "character_movie",
           joinColumns = {@JoinColumn(name = "movie_id")},
           inverseJoinColumns = {@JoinColumn(name = "character_id")})
   @JsonIgnore
    private List<Characterr> characters;

    public void addCharacter(Characterr character){
        characters.add(character);
    }
    public void addFranchise(Franchise franchise){
        setFranchise(franchise);
    }

    public void removeCharacter(Characterr character) {
        characters.remove(character);
    }

}
