package com.example.assignment6webapi.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Data
public class Characterr {

    /**
     * Columns for character table
     */

    @Id
    @Column(name = "character_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Integer id;

    @Column(name = "character_name", length = 50, nullable = false)
    private String fullName;

    @Column(name = "character_alias", length = 50, nullable = false)
    private String alias;

    @Column(name = "character_gender", length = 50, nullable = false)
    private String gender;

    @Column(name = "character_picture", length = 150, nullable = false)
    private String picture;

    /**
     * Basically means that a character can be featured in multiple movies
     */

    @ManyToMany(mappedBy = "characters")
    @JsonIgnore
    private List<Movie> movies;

}
