package com.example.assignment6webapi.repository;

import com.example.assignment6webapi.entities.Characterr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Characterr, Integer> {
}
