package com.example.assignment6webapi.mappers;


import com.example.assignment6webapi.dto.FranchiseDTO;
import com.example.assignment6webapi.entities.Franchise;
import com.example.assignment6webapi.entities.Movie;
import com.example.assignment6webapi.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This class help to convert from Entity class to DTO class and vice versa
 */
@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
   protected MovieService movieService;


    /**
     * convert from Entity to DTO
     * @param franchise
     * @return
     */
    @Mapping(target = "movieIds", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);
    public abstract List<FranchiseDTO> franchiseToFranchiseDTO(List<Franchise> franchise);


    /**
     * convert From DTO to Entity
     * @param franchiseDTO
     * @return
     */
    @Mapping(target = "movies", source = "movieIds", qualifiedByName = "moviesIdsToMovies")
    public abstract Franchise franchiseDTOToFranchise(FranchiseDTO franchiseDTO);


    @Named("moviesIdsToMovies")
    List<Movie> mapIdsToMovies(List<Integer> id) {
        return id.stream()
                .map( i -> movieService.findById(i))
                .collect(Collectors.toList());
    }

    @Named("moviesToIds")
    List<Integer> mapMoviesToIds(List<Movie> movies) {
        if(movies == null)
            return null;
        return movies.stream()
                .map(Movie::getId).collect(Collectors.toList());
    }

}
