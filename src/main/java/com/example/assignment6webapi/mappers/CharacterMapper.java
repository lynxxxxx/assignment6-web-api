package com.example.assignment6webapi.mappers;

import com.example.assignment6webapi.dto.CharacterDTO;
import com.example.assignment6webapi.entities.Characterr;
import com.example.assignment6webapi.entities.Movie;

import com.example.assignment6webapi.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This class help to convert from Entity class to DTO class and vice versa
 */
@Mapper(componentModel = "spring")
public abstract class CharacterMapper {

    @Autowired
    protected MovieService movieService;


    /**
     * convert from Entity to DTO
     * @param character
     * @return
     */
    @Mapping(target = "movieIds", source = "movies", qualifiedByName = "moviesToIds")
    public abstract CharacterDTO characterToCharacterDTO(Characterr character);
    public abstract List<CharacterDTO> characterToCharacterDTO(List<Characterr> characters);


    /**
     * convert From DTO to Entity
     * @param characterDTO
     * @return
     */
    @Mapping(target = "movies", source = "movieIds", qualifiedByName = "moviesIdsToMovies")
    public abstract Characterr characterDTOToCharacter(CharacterDTO characterDTO);
    public abstract List<Characterr> characterDTOToCharacter(List<CharacterDTO> characterDTO);


    @Named("moviesIdsToMovies")
    List<Movie> mapIdsToMovies(List<Integer> id) {
        return id.stream()
                .map( i -> movieService.findById(i))
                .collect(Collectors.toList());
    }


    @Named("moviesToIds")
    List<Integer> mapMoviesToIds(List<Movie> movies) {
        if(movies == null)
            return null;
        return movies.stream()
                .map(Movie::getId).collect(Collectors.toList());
    }


}
