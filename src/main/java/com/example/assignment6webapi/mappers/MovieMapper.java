package com.example.assignment6webapi.mappers;

import com.example.assignment6webapi.dto.MovieDTO;
import com.example.assignment6webapi.entities.Characterr;
import com.example.assignment6webapi.entities.Franchise;
import com.example.assignment6webapi.entities.Movie;
import com.example.assignment6webapi.exception.CharacterNotFoundException;
import com.example.assignment6webapi.services.character.CharacterService;
import com.example.assignment6webapi.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This class help to convert from Entity class to DTO class and vice versa
 */
@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
   protected CharacterService characterService;

    @Autowired
    protected FranchiseService franchiseService;

    /**
     * convert from Entity to DTO
     * @param movie
     * @return
     */
    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characterIds", source = "characters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDTO(Movie movie);
    public abstract List<MovieDTO> movieToMovieDTO(List<Movie> movie);


    /**
     * convert From DTO to Entity
     * @param movieDTO
     * @return
     */
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "characters", source = "characterIds", qualifiedByName = "charactersIdsToCharacters")
    public abstract Movie movieDTOToMovie(MovieDTO movieDTO);



    @Named("franchiseIdToFranchise")
    Franchise mapIdToFranchise(Integer id) {
        return franchiseService.findById(id);
    }



    @Named("charactersIdsToCharacters")
    List<Characterr> mapIdsToCharacters(List<Integer> id) {
        return id.stream()
                .map( i -> characterService.findById(i))
                .collect(Collectors.toList());
    }


    @Named("charactersToIds")
    List<Integer> mapCharactersToIds(List<Characterr> characters) {
        if(characters == null)
            return null;
        return characters.stream()
                .map(s -> s.getId()).collect(Collectors.toList());
    }
}
