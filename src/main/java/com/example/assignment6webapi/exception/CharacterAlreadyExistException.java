package com.example.assignment6webapi.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.ALREADY_REPORTED, reason = "CHARACTER_ALREADY_EXIST")
public class CharacterAlreadyExistException extends Exception{
}
