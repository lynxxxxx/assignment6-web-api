package com.example.assignment6webapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.PushBuilder;

@ResponseStatus(value = HttpStatus.ALREADY_REPORTED, reason = "MOVIE_ALREADY_EXIST")
public class MovieAlreadyExistException extends Exception{

}
