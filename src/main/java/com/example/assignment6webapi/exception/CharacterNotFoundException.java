package com.example.assignment6webapi.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "CHARACTER_NOT_FOUND")
public class CharacterNotFoundException extends Exception{
    public CharacterNotFoundException(Integer message) {
        super(message.toString());
    }
}
