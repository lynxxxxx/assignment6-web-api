package com.example.assignment6webapi.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "MOVIE_NOT_FOUND")
public class MovieNotFoundException extends  Exception{
    public MovieNotFoundException(Integer message){
        super(message.toString());
    }
}
