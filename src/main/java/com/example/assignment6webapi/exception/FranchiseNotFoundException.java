package com.example.assignment6webapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "FRANCHISE_NOT_FOUND")
public class FranchiseNotFoundException extends Exception{
    public FranchiseNotFoundException(Integer message) {
        super(message.toString());
    }
}
