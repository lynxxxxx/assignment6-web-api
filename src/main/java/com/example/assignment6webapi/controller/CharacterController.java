package com.example.assignment6webapi.controller;

import com.example.assignment6webapi.ApiErrorResponse;
import com.example.assignment6webapi.dto.CharacterDTO;
import com.example.assignment6webapi.entities.Characterr;
import com.example.assignment6webapi.exception.CharacterNotFoundException;
import com.example.assignment6webapi.exception.FranchiseNotFoundException;
import com.example.assignment6webapi.exception.MovieNotFoundException;
import com.example.assignment6webapi.mappers.CharacterMapper;
import com.example.assignment6webapi.services.character.CharacterService;
import com.example.assignment6webapi.services.character.CharacterServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import lombok.AllArgsConstructor;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/character")
@AllArgsConstructor
public class CharacterController {

   private final CharacterServiceImpl characterService;
   private final CharacterMapper characterMapper;


    @Operation(summary = "Get all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })


    @GetMapping("/get")
    public ResponseEntity getAllCharacter() {
        List<CharacterDTO> characterDTOS = characterMapper.characterToCharacterDTO(characterService.findAll());
        return ResponseEntity.ok(characterDTOS);
    }

    @Operation(summary = "Get a character by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable Integer id) {
        CharacterDTO characterDTO = characterMapper.characterToCharacterDTO(characterService.findById(id));
        return ResponseEntity.ok(characterDTO);
    }

    @Operation(summary = "Adds new character")
    @PostMapping
    public ResponseEntity add(
            @RequestBody Characterr characterr) {
        characterService.add(characterr);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "Updates a character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "character not found with supplied ID",
                    content = @Content)
    })

    @PutMapping("{id}")
    public ResponseEntity updateExisCharacter(
            @RequestBody CharacterDTO characterDTO,
            @PathVariable Integer id) {
        if(id != characterDTO.getId())
            return ResponseEntity.notFound().build();
        characterService.update(characterMapper.characterDTOToCharacter(characterDTO));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "delete a character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "character not found with supplied ID",
                    content = @Content)
    })

    @DeleteMapping("{id}")
    public ResponseEntity deleteById(@PathVariable Integer id) throws FranchiseNotFoundException, CharacterNotFoundException {
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
