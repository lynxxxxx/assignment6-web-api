package com.example.assignment6webapi.controller;

import com.example.assignment6webapi.ApiErrorResponse;
import com.example.assignment6webapi.dto.*;
import com.example.assignment6webapi.entities.Franchise;
import com.example.assignment6webapi.entities.Movie;
import com.example.assignment6webapi.mappers.CharacterMapper;
import com.example.assignment6webapi.mappers.FranchiseMapper;
import com.example.assignment6webapi.mappers.MovieMapper;
import com.example.assignment6webapi.services.franchise.FranchiseServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;




@RestController
@RequestMapping("/franchise")
@AllArgsConstructor
public class FranchiseController {

   private final FranchiseServiceImpl franchiseService;
   private  FranchiseMapper franchiseMapper;
  private  CharacterMapper characterMapper;


    @Operation(summary = "Get all franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })


    @GetMapping("/get")
    public ResponseEntity getAllFranchise() {
        List<FranchiseDTO> franchiseDTOS = franchiseMapper.franchiseToFranchiseDTO(franchiseService.findAll());
        return ResponseEntity.ok(franchiseDTOS);
    }

    @Operation(summary = "Get a Franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "franchise does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
            @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable Integer id) {
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDTO(franchiseService.findById(id));
        return ResponseEntity.ok(franchiseDTO);
    }

    @GetMapping("/get-movies/{id}")
    public ResponseEntity getAllMoviesByFranchiseId(@PathVariable Integer id) {
        FranchiseDTO franchise = franchiseMapper.franchiseToFranchiseDTO(franchiseService.getAllMoviesByFranchiseId(id));
        return ResponseEntity.ok(franchise.getMovieIds());
    }


    @GetMapping("/get-characters/{id}")
    public ResponseEntity getAllCharactersByFranchiseId(@PathVariable Integer id){
        List<CharacterDTO> characterDTO = characterMapper.characterToCharacterDTO(franchiseService.getAllCharactersByFranchiseId(id));
        return ResponseEntity.ok(characterDTO.stream().map(CharacterDTO::getId));
    }

    @Operation(summary = "Adds new Franchise")
    @PostMapping
    public ResponseEntity add(
            @RequestBody Franchise franchise) {
        franchiseService.add(franchise);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "Updates a Franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Student successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "franchise not found with supplied ID",
                    content = @Content)
    })

    @PutMapping("{id}")
    public ResponseEntity update(
            @RequestBody FranchiseDTO franchiseDTO, @PathVariable Integer id) {
        if (id != franchiseDTO.getId())
            return ResponseEntity.notFound().build();
        franchiseService.update(franchiseMapper.franchiseDTOToFranchise(franchiseDTO));
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}/updateMovie")
    public ResponseEntity<List<Movie>> updateMovieToFranchise( @PathVariable Integer id, @RequestBody List<Integer> movieId) {
       franchiseService.updateMovieToFranchise(id, movieId);
       return ResponseEntity.noContent().build();
    }

    @Operation(summary = "delete a Franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "franchise not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
