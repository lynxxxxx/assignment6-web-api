package com.example.assignment6webapi.controller;

import com.example.assignment6webapi.ApiErrorResponse;
import com.example.assignment6webapi.dto.MovieDTO;
import com.example.assignment6webapi.entities.Movie;
import com.example.assignment6webapi.mappers.MovieMapper;
import com.example.assignment6webapi.services.movie.MovieServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/movie")
@AllArgsConstructor

public class MovieController {
   private final MovieServiceImpl movieService;
   private  MovieMapper movieMapper;


    @Operation(summary = "Get all Movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    @GetMapping("/get")
    public ResponseEntity getAllMovie() {
       List<MovieDTO> movieDTOS = movieMapper.movieToMovieDTO(movieService.findAll());
       return ResponseEntity.ok(movieDTOS);
    }


    @Operation(summary = "Get a Movie by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    @GetMapping("/{id}")
    public ResponseEntity getMovieById(@PathVariable Integer id) {
        MovieDTO movieDTO = movieMapper.movieToMovieDTO(movieService.findById(id));
        return ResponseEntity.ok(movieDTO);
    }


    @GetMapping("get-characters/{movieId}")
    public ResponseEntity getAllCharactersByMovieId(@PathVariable Integer movieId) {
        MovieDTO movieDTO = movieMapper.movieToMovieDTO(movieService.getAllCharactersByMovieId(movieId));
        return ResponseEntity.ok(movieDTO.getCharacterIds());
    }

    @Operation(summary = "Adds new Movie")
    @PostMapping
    public ResponseEntity add(
            @RequestBody Movie movie) {
        movieService.add(movie);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }


@PutMapping("/add-character/{id}")
public ResponseEntity addCharacterToMovie(@PathVariable Integer id, @RequestBody List<Integer> characterId) {
    movieService.addCharacterToMovie(id,characterId);
    return ResponseEntity.noContent().build();
}

    @Operation(summary = "Updates a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })

    @PutMapping("{id}")
    public ResponseEntity update(
            @RequestBody MovieDTO movieDTO, @PathVariable Integer id) {
        if (id != movieDTO.getId())
            return ResponseEntity.notFound().build();
        movieService.update(movieMapper.movieDTOToMovie(movieDTO));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("remove-character/{id}")
    public ResponseEntity removeCharacterFromMovie(@PathVariable Integer id,
                                                   @RequestBody List<Integer> characterId) {
        movieService.removeCharacterFromMovie(id, characterId);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "delete a Movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}





//    @PutMapping("{movieId}/{characterId}")
//    public ResponseEntity addCharacterToMovie(@PathVariable Integer movieId, @PathVariable Integer characterId) {
//         movieMapper.movieToMovieDTO(movieService.addCharacterToMovie(movieId, characterId));
//        return ResponseEntity.noContent().build();
//    }