package com.example.assignment6webapi.services.character;
import com.example.assignment6webapi.entities.Characterr;
import com.example.assignment6webapi.exception.CharacterNotFoundException;
import com.example.assignment6webapi.repository.CharacterRepository;
import com.example.assignment6webapi.repository.MovieRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@AllArgsConstructor
public class CharacterServiceImpl implements CharacterService{
    CharacterRepository characterRepository;
    MovieRepository movieRepository;


    /**
     * User can find any character by using their id
     * @param id
     * @return
     */
    @Override
    public Characterr findById(Integer id) {
        return characterRepository.findById(id).orElse(null);
    }

    /**
     * Basically lists all characters in the character table
     * @return
     */
    @Override
    public List<Characterr> findAll() {
        return characterRepository.findAll();
    }

    /**
     * User can add new characters to the table
     * @param character
     * @return
     */
    @Override
    public Characterr add(Characterr character) {
        return characterRepository.save(character);
    }


    /**
     *User can update existing character in the table by using their id
     * @param character
     * @return
     */
    @SneakyThrows
    @Override
    public Characterr update(Characterr character) {
        Characterr characterGetOne = characterRepository.findById(character.getId()).orElseThrow(() -> new CharacterNotFoundException(character.getId()));
        characterGetOne.setFullName(character.getFullName());
        characterGetOne.setAlias(character.getAlias());
        characterGetOne.setGender(character.getGender());
        characterGetOne.setPicture(character.getPicture());
        return characterRepository.save(characterGetOne);
    }

    /**
     * User can delete any character in the table, but characterId must be removed in movie table
     * before deleting any character data by using their id.
     * @param id
     */
    @SneakyThrows
    @Override
    public void deleteById(Integer id){
        Characterr character = characterRepository.findById(id).orElseThrow(() -> new CharacterNotFoundException(id));
        character.getMovies().forEach(movie -> movie.removeCharacter(character));
        characterRepository.deleteById(id);
    }

    @Override
    public boolean exists(Integer integer) {
        return false;
    }
}
