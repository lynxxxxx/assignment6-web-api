package com.example.assignment6webapi.services.character;

import com.example.assignment6webapi.entities.Characterr;
import com.example.assignment6webapi.services.CrudService;
/**
 * CharacterService which is basically just an interface. Which forms a connection between the class and Impl class
 */
public interface CharacterService extends CrudService<Characterr, Integer> {
}
