package com.example.assignment6webapi.services.movie;

import com.example.assignment6webapi.entities.Characterr;
import com.example.assignment6webapi.entities.Movie;
import com.example.assignment6webapi.exception.CharacterAlreadyExistException;
import com.example.assignment6webapi.exception.CharacterNotFoundException;
import com.example.assignment6webapi.exception.MovieNotFoundException;
import com.example.assignment6webapi.repository.CharacterRepository;
import com.example.assignment6webapi.repository.FranchiseRepository;
import com.example.assignment6webapi.repository.MovieRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Service identifies a Java class that performs a certain function, such as calling external APIs, doing business logic, or completing calculations.
 * @AllArgsConstructor is an annotation that creates a constructor with one parameter for each class field.
 */
@Service
@AllArgsConstructor
public class MovieServiceImpl implements MovieService{

    MovieRepository movieRepository;
    FranchiseRepository franchiseRepository;
    CharacterRepository characterRepository;


    /**
     * This method gets All characters by using movieId
     * @param movieId
     * @return
     */

    @SneakyThrows
    public Movie getAllCharactersByMovieId(Integer movieId) {
        return movieRepository.findById(movieId).orElseThrow(() -> new MovieNotFoundException(movieId));
    }

    /**
     * add character to the movie charcter List if it no exist there
     * @param movieId
     * @param characterId
     */
    @SneakyThrows
    public void addCharacterToMovie(Integer movieId, List<Integer> characterId) {
        Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new MovieNotFoundException(movieId));
        List<Characterr> characters = movie.getCharacters();
        boolean contains;
        for (int id : characterId) {
            contains = characters.stream().anyMatch(a -> a.getId() == id);
            if (!contains) {
                if (characterRepository.existsById(id)) {
                    Characterr characters2 = characterRepository.findById(id).orElse(null);
                    movie.addCharacter(characters2);
                } else {
                    throw new CharacterNotFoundException(id);
                }
            }
            if(contains) {
                throw new CharacterAlreadyExistException();
            }
        }

        movieRepository.save(movie);
    }

    /**
     * Basically removes character from movie
     * @param movieId
     * @param characterId
     */
    @SneakyThrows
    public void removeCharacterFromMovie(Integer movieId, List<Integer> characterId) {
        Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new MovieNotFoundException(movieId));
        List<Characterr> characters = movie.getCharacters();
        boolean contains;
        for (int id : characterId) {
            contains = characters.stream().anyMatch(a -> a.getId() == id);
            if (contains) {
                if (characterRepository.existsById(id)) {
                    Characterr character = characterRepository.findById(id).orElse(null);
                    movie.removeCharacter(character);
                }
            }
            if(!contains) {
                throw new CharacterNotFoundException(id);
            }
        }

        movieRepository.save(movie);
    }


    /**
     * Any movie can be found by user inputting the movies id
     * @param id
     * @return
     */

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElse(null);
    }

    /**
     * User can get all movies displayed in this method
     * @return
     */
    @Override
    public List<Movie> findAll() {
        return movieRepository.findAll();
    }

    /**
     * User can add columns to the movie table
     * @param movie
     * @return
     */
    @Override
    public Movie add(Movie movie) {
        movieRepository.save(movie);
        return movie;

    }

    /**
     * User can update existing columns in the movie table with this method
     * @param movie
     * @return
     */
    @SneakyThrows
    @Override
    public Movie update(Movie movie) {
        Movie movieGetOne = movieRepository.findById(movie.getId()).orElseThrow(() -> new MovieNotFoundException(movie.getId()));
        movieGetOne.setMovieTitle(movie.getMovieTitle());
        movieGetOne.setGenre(movie.getGenre());
        movieGetOne.setRelease_year(movie.getRelease_year());
        movieGetOne.setDirector(movie.getDirector());
        movieGetOne.setMovie_picture(movie.getMovie_picture());
        movieGetOne.setMovie_trailer(movie.getMovie_trailer());
        return movieRepository.save(movieGetOne);
    }


    /**
     * User can basically delete any column from the movie table by using the movies id
     * @param id
     */

    @Override
    public void deleteById(Integer id) {
        movieRepository.deleteById(id);
    }

    @Override
    public boolean exists(Integer integer) {
        return false;
    }

}





//    publicList<Actor> updateActorsInMovie(IntegermovieId, Integer[] actorIds) {
//        Optional<Movie> repoMovie = movieRepository.findById(movieId);
//        Moviemovie = repoMovie.orElseThrow();
//        List<Actor> actors = movie.getActors();
//        booleancontains;
//        for (intid : actorIds) {
//            contains = actors.stream().anyMatch(actor -> actor.id == id);
//            if (!contains) {
//                if (actorRepository.existsById(id)) {
//                    Optional<Actor> actor = actorRepository.findById(id);
//                    actors.add(actor.orElse(null));
//                }
//            }
//        }
//        movie.setActors(actors);
//        movieRepository.save(movie);
//        returnmovie.getActors();
//    }

//    public List<Movie> updateMoviesInFranchise(Integer franchiseId, Integer[] moviesIds) {
//        Optional<Franchise> repoFranchise = franchiseRepository.findById(franchiseId);
//        Franchise franchise = repoFranchise.orElseThrow();
//        List<Movie> movies = franchise.getMovies();
//        boolean contains;
//        for (int id : moviesIds) {
//            contains = movies.stream().anyMatch(movie -> movie.id == id);
//            if (!contains) {
//                if (movieRepository.existsById(id)) {
//                    Optional<Movie> movieRepo = movieRepository.findById(id);
//                    Movie movie = movieRepo.get();
//                    movie.franchise = franchise;
//                    movies.add(movie);
//                }
//            }
//        }
//        franchise.setMovies(movies);
//        franchiseRepository.save(franchise);
//        return franchise.getMovies();
// ERGR   }
