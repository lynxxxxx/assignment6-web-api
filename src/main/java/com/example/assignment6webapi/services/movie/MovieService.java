package com.example.assignment6webapi.services.movie;

import com.example.assignment6webapi.entities.Movie;
import com.example.assignment6webapi.services.CrudService;

/**
 * MovieService which is basically just an interface. Which forms a connection between the class and Impl class
 */
public interface MovieService extends CrudService<Movie, Integer> {
}
