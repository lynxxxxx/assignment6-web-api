package com.example.assignment6webapi.services;

import com.example.assignment6webapi.entities.Characterr;
import com.example.assignment6webapi.exception.CharacterNotFoundException;
import com.example.assignment6webapi.exception.FranchiseNotFoundException;
import com.example.assignment6webapi.exception.MovieNotFoundException;

import java.util.List;

/**
 * Generic service to act as a parent to all domain-specific services.
 * It encapsulates basic CRUD functionality to be reused.
 * It follows the same structure as JPA Repositories to ease integration.
 * @param <T> Type of domain class of the service.
 * @param <ID> Primary key type for entity.
 */
public interface CrudService <T, ID> {
    T findById(ID id);
    List<T> findAll();
    T add(T entity);
    T update(T entity);
    void deleteById(ID id);
    boolean exists(ID id);

}

