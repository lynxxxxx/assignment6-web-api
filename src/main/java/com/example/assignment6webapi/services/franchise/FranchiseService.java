package com.example.assignment6webapi.services.franchise;

import com.example.assignment6webapi.entities.Franchise;
import com.example.assignment6webapi.services.CrudService;
/**
 * FranchiseService which is basically just an interface. Which forms a connection between the class and Impl class
 */
public interface FranchiseService extends CrudService<Franchise, Integer> {
}
