package com.example.assignment6webapi.services.franchise;

import com.example.assignment6webapi.entities.Characterr;
import com.example.assignment6webapi.entities.Franchise;
import com.example.assignment6webapi.entities.Movie;
import com.example.assignment6webapi.exception.FranchiseNotFoundException;
import com.example.assignment6webapi.exception.MovieAlreadyExistException;
import com.example.assignment6webapi.exception.MovieNotFoundException;
import com.example.assignment6webapi.repository.FranchiseRepository;
import com.example.assignment6webapi.repository.MovieRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class FranchiseServiceImpl implements FranchiseService{
    FranchiseRepository franchiseRepository;
    MovieRepository movieRepository;


    /**
     * This method gets All Movies by using Franchise id as they have a relation
     * @param
     * @return
     */
    @SneakyThrows
    public Franchise getAllMoviesByFranchiseId(Integer id){
        return franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
    }

    /**
     * Gets all character by franchise id
     * @param id
     * @return
     */
    @SneakyThrows
    public List<Characterr> getAllCharactersByFranchiseId(Integer id) {
        Franchise franchise = franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
        List<Characterr> characters = new ArrayList<>();
        for (int movie = 0; movie < franchise.getMovies().size(); movie++) {
            characters.addAll(franchise.getMovies().get(movie).getCharacters());
        }
        return characters;

    }

    /**
     * Finds franchise by id
     * @param franchiseId
     * @return
     */
   
    @Override
    public Franchise findById(Integer franchiseId) {
        return franchiseRepository.findById(franchiseId).orElse(null);
    }

    /**
     * Lists all franchise data
     * @return
     */
    @Override
    public List<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    /**
     * User can add new franchise to table
     * @param franchise
     * @return
     */
    @Override
    public Franchise add(Franchise franchise) {
        franchiseRepository.save(franchise);
        return franchise;
    }

    /**
     * User can update existing data by using their Id
     * @param franchise
     * @return
     */

    @SneakyThrows
    @Override
    public Franchise update(Franchise franchise) {
        Franchise franchiseGetOne = franchiseRepository.findById(franchise.getId()).orElseThrow(() -> new FranchiseNotFoundException(franchise.getId()));
        franchiseGetOne.setFranchiseName(franchise.getFranchiseName());
        franchiseGetOne.setDescription(franchise.getDescription());
        return franchiseRepository.save(franchiseGetOne);

    }

    /**
     * User can delete any franchise data by using their id. However, franchiseId must be removed in movie
     * table before deleting any franchise data by using their id
     * @param id
     */
    @SneakyThrows
    @Override
    public void deleteById(Integer id) {
        Franchise franchise = franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
        franchise.getMovies().forEach(movie -> movie.setFranchise(null));
        franchiseRepository.deleteById(id);

    }

    @Override
    public boolean exists(Integer integer) {
        return false;
    }

    /**
     * add movie to franchise movie List if it no exist there
     * @param franchiseId
     * @param movieId
     */
    @SneakyThrows
    public void updateMovieToFranchise(Integer franchiseId, List<Integer> movieId) {
        Franchise franchise = franchiseRepository.findById(franchiseId).orElseThrow();
        List<Movie> movies = franchise.getMovies();
        boolean contains;
        for (int id : movieId) {
            contains = movies.stream().anyMatch(movie -> movie.getId() == id);
            if (!contains) {
                if (movieRepository.existsById(id)) {
                    Movie movie = movieRepository.findById(id).orElseThrow(() -> new MovieNotFoundException(id));
                    movie.addFranchise(franchise);
                    franchise.updateMovieToFranchise(movie);
                } else {
                    throw new MovieNotFoundException(id);
                }
            }
            if(contains){
              throw new MovieAlreadyExistException();
            }
        }
        franchiseRepository.save(franchise);
    }

}

