# Creating a Web API and database with Spring

## Background
Movie Characters API

Hibernate was used to generate a postgreSQL database that we had to expose utilizing a deployed Web API. Utilizing PostgreSQL from the ground up, the database implements seeder data during application startup. We developed a REST API in Java and documented it using Swagger.

## Installation

Requirements:

- IntelliJ with Java 17, Spring Web, Spring Data JPA
- Docker
- Swagger (optional)
- Start up Docker
- Run the following command in a terminal
- docker run -d --name ***** -e POSTGRES_PASSWORD=****** -e POSTGRES_DB=****** -p 5432:5432 
- Open the downloaded or cloned repository in your IDE
- Run the app
- Navigate to http://localhost:8080/swagger-ui/index.html#
- Start testing the endpoints

## Business rules 

- Characters and movies

One movie contains many characters, and a character can play in multiple movies.

- Movies and franchises

One movie belongs to one franchise, but a franchise can contain many movies

## API requirements

Generic CRUD- Create, Read, Update, & Delete

Full CRUD is expected for **Movies**, **Characters**, and **Franchises**. You can do proper deletes, just ensure related
data is not deleted – the foreign keys can be set to null (business logic). This means that Movies can have no
Characters, and Franchises can have no Movies.

## How To Use

- Entity details

We had to develop a Franchise, Character, and Movie entity for the task. Characterr(yes with 2 r's) was chosen to replace the Character entity class since Character is already a Java class.

- Endpoints for API - SwaggerIO

You will see the Swagger interface as soon as the program launches. You may test the various endpoints. For example: 

- Endpoints PUT

For each PUT endpoint pointing to an Character, movie, or franchise update.

- POST Endpoints 

For each POST endpoint pointing to the creation of a certain movie, character, or franchise.

## Built With

- IntelliJ IDEA
- Spring Framework
- Spring Data JPA
- Springdoc OpenAPI
- Swagger
- PostgreSQL with PgAdmin 
- Docker


## Contributors

@Lynxxxxx
@DanSho
